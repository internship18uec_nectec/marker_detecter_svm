#!/usr/bin/env python
# ! -*- coding: utf-8 -*-

from __future__ import print_function

import dlib
import rospy
import numpy as np
import cv2
import time
import roslib
roslib.load_manifest('marker_detector_svm')
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

# from functools import wraps

# def stop_watch(func):
#     @wraps(func)
#     def wrapper(*args, **kargs):
#         start = time.time()
#         result = func(*args, **kargs)
#         elapsed_time = time.time() - start
#         print("{}".format(elapsed_time))
#         return result
#     return wrapper


def callback(data):
    global svm_path, sum_time, counter, detector, image_pub
    # Start streaming

    if (counter >= 516):
        return
    try:
        bridge = CvBridge()
        color_image = bridge.imgmsg_to_cv2(data, "bgr8")
        # Wait for a coherent pair of frames: depth and color
        # if not color_frame:
        #     pass
        # Convert images to numpy arrays
        # color_image = np.asanyarray(color_frame.get_data())
        out = color_image.copy()

        # **************************
        start = time.time()
        dets = detector(color_image)
        elapsed_time = time.time() - start
        # **************************
        print("{} sec, {}".format(elapsed_time, len(dets)))
        sum_time += elapsed_time
        counter += 1
        # 四角形描写
        if len(dets) > 0:
            cv2.rectangle(out,
                          (dets[0].left(), dets[0].top()),
                          (dets[0].right(), dets[0].bottom()),
                          (0, 255, 0), 2)
        msg = bridge.cv2_to_imgmsg(out, "bgr8")
        image_pub.publish(msg)
        # for d in dets:
        # cv2.rectangle(out,
        #               (d.left(), d.top()), (d.right(), d.bottom()),
        #               (0, 0, 255), 2)

        # Show images
        # cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        # cv2.imshow('RealSense', out)
        # key = cv2.waitKey(1) & 0xff
        # if key == ord('q'):
        #     pass
        #     # exit()
    except CvBridgeError as e:
        print(e)
    finally:
        pass
        # Stop streaming
        # cv2.destroyAllWindows()


def main():
    global bridge, svm_path, sum_time, counter, detector, image_pub
    print("debug")
    sum_time = 0
    counter = 0

    svm_path = rospy.get_param('~svm_path',
                               '/home/cde/catkin_ws/src/marker_detector_svm/'
                               'models/svm/detector.svm')
    detector = dlib.simple_object_detector(svm_path)

    print("debug")
    print("debug")
    rospy.Subscriber("/camera/color/image_rect_color", Image, callback)
    image_pub = rospy.Publisher("/mysvm/color/image_rect_color", Image)

    print("debug")
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    try:
        rospy.init_node('ros_svmdetecter', anonymous=True)
        main()
        print("Number item : \t{}times".format(counter))
        print("Total time :  \t{} sec".format(sum_time))
        print("Average :     \t{} sec".format(sum_time/counter))
    except rospy.ROSInterruptExceptione:
        pass
