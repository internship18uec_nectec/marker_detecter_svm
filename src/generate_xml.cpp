#include <ros/ros.h>
#include <fstream>
#include <iostream>
#include <string>
using namespace std;
int main(int argc, char *argv[]) {
  ros::init(argc, argv, "generate_xml");
  ros::NodeHandle pnh = ros::NodeHandle("~");
  std::string traintxt_path;  // path to marker image
  pnh.param<std::string>("traintxt_path", traintxt_path,
                         "/home/cde/catkin_ws/src/sample_realsensed435/data/"
                         "raw/train_data1/train.txt");
  ifstream ifs(traintxt_path);
  string filename;
  int num;
  cout << "<?xml version='1.0' encoding='ISO-8859-1'?>" << endl
       << "<?xml-stylesheet type='text/xsl' "
          "href='image_metadata_stylesheet.xsl'?>"
       << endl
       << "<dataset>" << endl
       << "<name>imglab dataset</name>" << endl
       << "<images>" << endl;
  while (ifs >> filename >> num && num) {
    cout << "<image file=\'" << filename << "\'>" << endl;
    for (int i = 0; i < num; ++i) {
      int x, y, width, height;
      ifs >> x >> y >> width >> height;
      cout << "<box top=\'" << y << "\' left=\'" << x << "\' width=\'" << width
           << "\' height=\'" << height << "\'/>" << endl;
    }
    cout << "</image>" << endl;
  }
  cout << "</images>" << endl;
  cout << "</dataset>" << endl;
  return 0;
}