#!/usr/bin/env python
# ! -*- coding: utf-8 -*-

import dlib
import rospy
import pyrealsense2 as rs
import numpy as np
import cv2

# from functools import wraps
import time


# def stop_watch(func):
#     @wraps(func)
#     def wrapper(*args, **kargs):
#         start = time.time()
#         result = func(*args, **kargs)
#         elapsed_time = time.time() - start
#         print("{}".format(elapsed_time))
#         return result
#     return wrapper

def main():
    svm_path = rospy.get_param('~svm_path',
                               '/home/cde/catkin_ws/src/marker_detector_svm/'
                               'models/svm/detector.svm')
    # Configure depth and color streams
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

    # Start streaming
    pipeline.start(config)

    detector = dlib.simple_object_detector(svm_path)
    try:
        sum = 0
        a = 0
        while True:
            start = time.time()
            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()
            color_frame = frames.get_color_frame()
            if not color_frame:
                continue
            # Convert images to numpy arrays
            color_image = np.asanyarray(color_frame.get_data())
            out = color_image.copy()

            dets = detector(color_image)
            elapsed_time = time.time() - start
            print("{} sec, {}".format(elapsed_time, len(dets)))
            sum += elapsed_time
            a += 1
            # 四角形描写
            if len(dets) > 0:
                cv2.rectangle(out,
                              (dets[0].left(), dets[0].top()),
                              (dets[0].right(), dets[0].bottom()),
                              (0, 0, 255), 2)

            # for d in dets:
            # cv2.rectangle(out,
            #               (d.left(), d.top()), (d.right(), d.bottom()),
            #               (0, 0, 255), 2)

            # Show images
            cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('RealSense', out)
            key = cv2.waitKey(1) & 0xff
            if key == ord('q'):
                print("Average: {} sec".format(sum/a))
                break
                # exit()
    finally:
        # Stop streaming
        pipeline.stop()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
