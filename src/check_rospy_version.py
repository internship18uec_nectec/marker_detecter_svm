#!/usr/bin/env python
# license removed for brevity
import rospy 

def main():
    rospy.init_node('Python')

    while not rospy.is_shutdown():
        str = "hello world %s"%rospy.get_time()
        rospy.loginfo(str)
        print(3/2)

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException: pass

